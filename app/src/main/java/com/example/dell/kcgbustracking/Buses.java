package com.example.dell.kcgbustracking;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by karthik on 25/2/17.
 */

public class Buses extends android.support.v4.app.Fragment {


    CardView bus1,bus2,bus3,bus4,bus5,bus6,bus7,bus8,bus9,bus10,bus11,bus12,bus13,bus14,bus15,bus16,bus17,bus18,bus19,bus20;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.buses, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Buses");
        bus1 = (CardView) getView().findViewById(R.id.bus1);
        bus2 = (CardView) getView().findViewById(R.id.bus2);
        bus3 = (CardView) getView().findViewById(R.id.bus3);
        bus4 = (CardView) getView().findViewById(R.id.bus4);
        bus5 = (CardView) getView().findViewById(R.id.bus5);
        bus6 = (CardView) getView().findViewById(R.id.bus6);
        bus7 = (CardView) getView().findViewById(R.id.bus7);
        bus8 = (CardView) getView().findViewById(R.id.bus8);
        bus9 = (CardView) getView().findViewById(R.id.bus9);
        bus10 = (CardView) getView().findViewById(R.id.bus10);
        bus11 = (CardView) getView().findViewById(R.id.bus11);
        bus12 = (CardView) getView().findViewById(R.id.bus12);
        bus13 = (CardView) getView().findViewById(R.id.bus13);
        bus14 = (CardView) getView().findViewById(R.id.bus14);
        bus15 = (CardView) getView().findViewById(R.id.bus15);
        bus16 = (CardView) getView().findViewById(R.id.bus16);
        bus17 = (CardView) getView().findViewById(R.id.bus17);
        bus18 = (CardView) getView().findViewById(R.id.bus18);
        bus19 = (CardView) getView().findViewById(R.id.bus19);
        bus20 = (CardView) getView().findViewById(R.id.bus20);




        bus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    myroute.i=0;
                    ((MainActivity)getActivity()).setval("1");
                    ((MainActivity)getActivity()).mapact();

            }
        });
        bus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("2");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("3");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("4");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("5");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("6");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("7");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("8");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("9");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("10");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("11");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("12");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("13");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("14");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("15");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("16");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus17.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("17");
                ((MainActivity)getActivity()).mapact();
            }
        });

        bus18.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("18");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus19.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("19");
                ((MainActivity)getActivity()).mapact();
            }
        });
        bus20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myroute.i=0;
                ((MainActivity)getActivity()).setval("20");
                ((MainActivity)getActivity()).mapact();
            }
        });
    }




}
