package com.example.dell.kcgbustracking;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by karthik on 25/2/17.
 */

public class Home extends android.support.v4.app.Fragment {
    TextView buscard,mroute;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.home_try, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Home");
       buscard = (TextView) getView().findViewById(R.id.buscard);
        mroute = (TextView) getView().findViewById(R.id.myroute);
      // drivercard  = (CardView) getView().findViewById(R.id.cardView2);
       // contactcard = (CardView) getView().findViewById(R.id.cardView4);
        //favouritecard = (CardView) getView().findViewById(R.id.cardView3);

        buscard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).displaySelectedScreen(R.id.nav_buses);

            }
        });

        mroute.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myroute.edit ==0)
                {
                    ((MainActivity)getActivity()).chosetoast();
                    myroute.edit += 1;
                    ((MainActivity)getActivity()).displaySelectedScreen(R.id.nav_myroute);
                }
                else{
                    ((MainActivity)getActivity()).displaySelectedScreen(R.id.nav_buses);

                }

            }
        });

       /* contactcard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ((MainActivity)getActivity()).displaySelectedScreen(R.id.nav_contact);

            }
        });*/

       /* favouritecard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if(myroute.selected==0){
                    choosebus();
                }

            }
        });*/
    }

    public void choosebus(){
        ((MainActivity)getActivity()).routealert();
    }
}
