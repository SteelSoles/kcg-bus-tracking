package com.example.dell.kcgbustracking;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {


    Location location = null;
    private GoogleMap mMap;
    // static int i=0;
    String busval;
    //CardView cardView;
   public static LatLng coords;
    // int delay = 0; // delay for 0 sec.
    //int period = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

    }

    /*private void delayfn() {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask()
        {
            public void run()
            {
               location.setLatitude(location.getLatitude()+1.0);
                location.setLongitude(location.getLongitude()+5.0);//Call function
            }
        }, delay, period);
    }
*/

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        busval =myroute.busno;
        Toast.makeText(MapsActivity.this,myroute.busno,Toast.LENGTH_SHORT).show();

        //location.setLatitude(13.011545);
        //location.setLongitude(80.210067);
        // Add a marker in Sydney and move the camera
       LatLng sydney = new LatLng(13.011545,80.210067);
       // mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10));
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coords,18));
                //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coords,17));

                return false;
            }
        });

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                new MapsActivity.Getdata().execute();
            }
        };
        timer.scheduleAtFixedRate(timerTask, 0, 3000);

        // delayfn();
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        myroute.i=0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        myroute.i=0;
    }

    @Override
    protected void onPause() {
        super.onPause();
        myroute.i=0;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        myroute.i=0;
        new Getdata().execute();

    }

    class Getdata extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection conn = null;
            URL url = null;
            try {
                url = new URL("http://58.68.70.126:9999/fetchdata.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                StringBuilder busnumb= new StringBuilder();
                busnumb.append(URLEncoder.encode("id", "UTF-8"));
                busnumb.append("=");
                busnumb.append(URLEncoder.encode(busval, "UTF-8"));
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                try {
                    writer.write(busnumb.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                writer.flush();
                writer.close();
                os.close();
                conn.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {

                int response_code = conn.getResponseCode();

                // Check if successful connection made
                if (response_code == HttpURLConnection.HTTP_OK) {

                    // Read data sent from server
                    InputStream input = conn.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(input));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    // Pass data to onPostExecute method
                    return (result.toString());

                } else {

                    return ("unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
                return "exception";
            } finally {
                conn.disconnect();
            }
        }

        @Override
        protected void onPostExecute(String result) {

           // Toast.makeText(MapsActivity.this,result+busval,Toast.LENGTH_SHORT).show();
            try {
                JSONObject c = new JSONObject(result);
                JSONArray jsonArray = c.getJSONArray("result");
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                String lati = jsonObject.getString("latitude");
                String longi = jsonObject.getString("longitude");
                double lat = Double.parseDouble(lati);
                double lng = Double.parseDouble(longi);
                mMap.clear();
                coords = new LatLng(lat,lng);
                float zoom = mMap.getCameraPosition().zoom;
                  Marker marker=mMap.addMarker(new MarkerOptions().position(coords)
                          .title("Bus location")
                            .icon((BitmapDescriptorFactory.fromResource(R.drawable.busicedit1))));
              /* mMap.addCircle(new CircleOptions()
                        .center(coords)
                        .radius(10)
                        .strokeColor(Color.WHITE)
                        .fillColor(0xff00ddff));*/


                if(myroute.i == 0){
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(coords));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coords,17));
                   myroute.i=1;
                }
                else{
                    //if(zoom<=16)
                    //mMap.addMarker(new MarkerOptions().position(coords).title("Bus location"));
                    coords = mMap.getCameraPosition().target;
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(coords));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(coords,zoom));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
