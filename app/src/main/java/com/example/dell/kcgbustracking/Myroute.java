package com.example.dell.kcgbustracking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


/**
 * Created by karthik on 25/3/17.
 */

public class Myroute extends android.support.v4.app.Fragment {

    String[] buses={"Avadi","Kalpakkam","Avadi","Mugappair","Thiruvotriyur","Periyar","Purasaiwalkam","Mylapore","Triplicane","Porur","Koyambedu","ECR","West Mambalam","Tambaram","Tambaram II","Madipakkam I","Madipakkam II","Velachery","Kodambakkam","East Tambaram"};

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments

        return inflater.inflate(R.layout.myroute, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("My Route");
        ArrayAdapter adapter = new ArrayAdapter<String>((MainActivity)getActivity(), R.layout.listv, buses);

        ListView listView = (ListView) getView().findViewById(R.id.listview);
        listView.setAdapter(adapter);


    }
}
