package com.example.dell.kcgbustracking;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by karthik on 3/3/17.
 */

public class choose_bus extends android.support.v4.app.Fragment {
    String[] mobileArray = {"Chengalpet","Kalpakkam","Avadi","Mugappair","Thiruvotriyur","Periyar Nagar","Purasaiwalkam","Mylapore",
            "Triplicane","Porur","Koyambedu","ECR","West Mambalam","Tambaram I","Tambaram II","Madipakkam II",
            "Madipakkam I","Velacherry","Kodambakkam","East Tambaram"

    };
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        return inflater.inflate(R.layout.choose_bus, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Choose Route");


    }
}
